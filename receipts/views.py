from django.shortcuts import render
from django.views.generic.list import ListView
from django.shortcuts import redirect
from receipts.models import Account, ExpenseCategory, Receipt
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)
